---
title: "R collaboration flow map"
author: "Stéphane Guillou"
date: "2020-04-15"
output:
  html_document:
    keep_md: yes
---



## The problem

A reference list from Web of Science provides authorship information with all authors affiliations lumped into one single cell.

This worked example looks at extracting the author affiliations, joining the affiliation coordinates, and visualising the collaborations between institutions on a world map.

To achieve this, we use two tools: the R programming language for statistics, and OpenRefine.

## Data munging

### Load necessary functions

We will be using many of the core tidyverse packages to clean, transform and analyse the data.


```r
library(tidyverse)
```

```
## ── Attaching packages ───────────────────────────────────────────────────────────── tidyverse 1.3.0 ──
```

```
## ✓ ggplot2 3.3.0     ✓ purrr   0.3.3
## ✓ tibble  3.0.0     ✓ dplyr   0.8.5
## ✓ tidyr   1.0.2     ✓ stringr 1.4.0
## ✓ readr   1.3.1     ✓ forcats 0.5.0
```

```
## ── Conflicts ──────────────────────────────────────────────────────────────── tidyverse_conflicts() ──
## x dplyr::filter() masks stats::filter()
## x dplyr::lag()    masks stats::lag()
```

### Read spreadsheet

Our original data comes from Web of Science. Let's read it into an object:


```r
wos_raw <- read_csv("wos_refs.csv")
```

What does the data look like?


```r
wos_raw
```

```
## # A tibble: 256 x 68
##    PT    AU    BA    BE    GP    AF    BF    CA    TI    SO    SE    BS    LA   
##    <chr> <chr> <lgl> <chr> <lgl> <chr> <lgl> <chr> <chr> <chr> <chr> <lgl> <chr>
##  1 J     Kuo,… NA    <NA>  NA    Kuo,… NA    <NA>  In v… EURO… <NA>  NA    Engl…
##  2 J     Imam… NA    <NA>  NA    Imam… NA    <NA>  Intr… EURO… <NA>  NA    Engl…
##  3 J     Cheu… NA    <NA>  NA    Cheu… NA    <NA>  The … INTE… <NA>  NA    Engl…
##  4 J     Xu, … NA    <NA>  NA    Xu, … NA    <NA>  Semi… JOUR… <NA>  NA    Engl…
##  5 J     Knoe… NA    <NA>  NA    Knoe… NA    <NA>  Shea… PROC… <NA>  NA    Engl…
##  6 J     Fros… NA    <NA>  NA    Fros… NA    <NA>  Anal… MSPH… <NA>  NA    Engl…
##  7 J     Atac… NA    <NA>  NA    Atac… NA    <NA>  DNA … FASE… <NA>  NA    Engl…
##  8 J     Jen,… NA    <NA>  NA    Jen,… NA    <NA>  Neis… ACS … <NA>  NA    Engl…
##  9 J     Tran… NA    <NA>  NA    Tran… NA    <NA>  Spec… BIOC… <NA>  NA    Engl…
## 10 J     Atac… NA    <NA>  NA    Atac… NA    <NA>  Non-… SCIE… <NA>  NA    Engl…
## # … with 246 more rows, and 55 more variables: DT <chr>, CT <chr>, CY <chr>,
## #   CL <chr>, SP <lgl>, HO <lgl>, DE <chr>, ID <chr>, AB <chr>, C1 <chr>,
## #   RP <chr>, EM <chr>, RI <chr>, OI <chr>, FU <chr>, FX <chr>, CR <lgl>,
## #   NR <dbl>, TC <dbl>, Z9 <dbl>, U1 <dbl>, U2 <dbl>, PU <chr>, PI <chr>,
## #   PA <chr>, SN <chr>, EI <chr>, BN <chr>, J9 <chr>, JI <chr>, PD <chr>,
## #   PY <dbl>, VL <dbl>, IS <chr>, PN <dbl>, SU <dbl>, SI <chr>, MA <lgl>,
## #   BP <chr>, EP <chr>, AR <chr>, DI <chr>, D2 <chr>, EA <chr>, PG <dbl>,
## #   WC <chr>, SC <chr>, GA <chr>, UT <chr>, PM <dbl>, OA <chr>, HC <chr>,
## #   HP <chr>, DA <chr>, Column1 <lgl>
```

That's a lot of variables. How many?


```r
ncol(wos_raw)
```

```
## [1] 68
```

We to do some cleaning up.

### Clean up data

We only keep the variables that matter to us:


```r
wos <- wos_raw %>% 
  select(DOI = DI, auth_aff = C1)
```

Currently, the authorship data for one journal article is all lumped together in one single string. Here is an example:


```r
wos[[2]][1]
```

```
## [1] "[Kuo, Andy; Magiera, Julia; Rethwan, Nursyazwani; Lam, Ai Leen; Smith, Maree] Univ Queensland, Fac Med, Sch Biomed Sci, Brisbane, Qld, Australia; [Andersson, Asa; Wyse, Bruce; Lewis, Richard] Univ Queensland, Inst Mol Biosci, Brisbane, Qld, Australia; [Meutermans, Wim] VAST Biosci Pty Ltd, Brisbane, Qld, Australia"
```

Thankfully, it is structured! Author names are inside square brackets `[` and `]`, followed by their affiliation. The main name of the affiliation is at the beginning, followed by a comma `,`. Finally, each unit of author list + corresponding affiliation is separated by a semicolon `;`.

We will now use that consistent data structure to clean up, then split the authorship data, and then clean up some more:


```r
wos_split <- wos %>% 
  # remove rows with no data about authorship
  # (they happen to all be corrections)
  filter(!is.na(auth_aff)) %>% 
  # remove groups of authors
  mutate(aff_long = str_remove_all(auth_aff, "\\[.*?\\]")) %>% 
  # separate each affiliation across rows
  separate_rows(aff_long, sep = ";") %>% 
  # remove leading and trailing whitespace
  mutate(aff_long = str_trim(aff_long)) %>% 
  # only keep main element (i.e. first element)
  mutate(aff = str_extract(aff_long, "^.*?(?=,)"))
```

We now have single affiliations in the last columns. However, we have repeats, like "UQ" and "Univ Queensland".

Let's first find the acronyms:


```r
wos_split %>% 
  group_by(aff) %>% 
  # keep how many times the name is found, to gauge relevance
  tally() %>%
  # find the length of affiliation names
  mutate(chars = str_length(aff)) %>% 
  # sort them to find the shortest, likely to be acronyms
  arrange(chars) %>% 
  head(20)
```

```
## # A tibble: 20 x 3
##    aff       n chars
##    <chr> <int> <int>
##  1 MU        1     2
##  2 UQ        5     2
##  3 CNR       2     3
##  4 ETH       1     3
##  5 IEO       1     3
##  6 MMV       1     3
##  7 NYU       2     3
##  8 TUM       7     3
##  9 VIB       1     3
## 10 CHUV      1     4
## 11 CNRS      4     4
## 12 CSIC      5     4
## 13 CSIR      3     4
## 14 CUNY      1     4
## 15 DESY      1     4
## 16 EMBL      1     4
## 17 IMCB      1     4
## 18 INEM      1     4
## 19 INRA      1     4
## 20 NCTR      1     4
```

The main problem seems to be UQ and TUM (assumed to be Tech Uni Munich).
There is another inconsistency in which the German acronym is appended to the English version of the name.

We can fix this small number of issues by hand:


```r
# create a list of replacements
fixes <- c("^UQ" = "Univ Queensland",
           "^TUM" = "Tech Univ Munich",
           "^Helmholtz Ctr Infect Res HZI" = "Helmholtz Ctr Infect Res")
# replace the acronyms
wos_clean <- wos_split %>% 
  mutate(aff = str_replace_all(aff, fixes))
```


### Explore the data

What are the most common affiliations?


```r
aff_count <- wos_clean %>% 
  group_by(aff) %>% 
  tally(sort = TRUE)
```

This data could be used for the size of the nodes... Let's have a look at the first few rows:


```r
head(aff_count)
```

```
## # A tibble: 6 x 2
##   aff                  n
##   <chr>            <int>
## 1 Univ Queensland    333
## 2 Griffith Univ      147
## 3 Univ Melbourne      42
## 4 Monash Univ         22
## 5 Chinese Acad Sci    18
## 6 Emory Univ          15
```

How many institutions are listed?


```r
nrow(aff_count)
```

```
## [1] 485
```

How many authors on each publication?


```r
wos_clean %>% 
  group_by(DOI) %>% 
  tally(sort = TRUE)
```

```
## # A tibble: 252 x 2
##    DOI                               n
##    <chr>                         <int>
##  1 10.1080/15548627.2015.1100356   353
##  2 10.1080/15548627.2018.1465784    46
##  3 10.1080/15548627.2019.1634444    43
##  4 10.1038/s41588-019-0417-8        30
##  5 10.1126/scitranslmed.aaf1471     22
##  6 10.1016/j.vaccine.2019.03.059    20
##  7 10.1073/pnas.1809100115          16
##  8 10.1038/s41564-017-0015-4        14
##  9 10.1093/cid/ciz099               14
## 10 10.1016/j.immuni.2016.08.010     13
## # … with 242 more rows
```

Note that one publication has 353 authors, which would have a lot more weight than others!

We can use fractional counting instead, so large authorships don't have as much weight:


```r
aff_frac_count <- wos_clean %>% 
  # first, get number of authors in each paper
  group_by(DOI) %>% 
  mutate(size = n()) %>%
  # then, get fractional count for each insitution in each paper
  group_by(DOI, aff) %>% 
  summarise(frac_n = n() / nth(size, 1)) %>%
  # finally, sum all fractional counts for each institution
  group_by(aff) %>% 
  summarise(frac_n = sum(frac_n))
```

This will be used for the size of the nodes instead, and for picking the most important institutions.

We can keep both n and frac_n in one single dataset:


```r
aff_stats <- left_join(aff_frac_count, aff_count)
```

```
## Joining, by = "aff"
```

And we end up with a dataset organise like this:


```r
head(aff_stats)
```

```
## # A tibble: 6 x 3
##   aff                                        frac_n     n
##   <chr>                                       <dbl> <int>
## 1 Aarhus Univ                               0.0113      4
## 2 Aarhus Univ Hosp                          0.00283     1
## 3 Acad Athens                               0.00567     2
## 4 Acad Sinica                               0.196       6
## 5 Adam Mickiewicz Univ                      0.00283     1
## 6 Agr & Food Commonwealth Sci & Ind Res Org 0.167       2
```

Are there references with only one institution?


```r
wos_clean %>% 
  group_by(DOI) %>% 
  summarise(n = length(unique(aff))) %>% 
  arrange(n)
```

```
## # A tibble: 252 x 2
##    DOI                                    n
##    <chr>                              <int>
##  1 10.1002/cbic.201500081                 1
##  2 10.1002/jps.24497                      1
##  3 10.1002/prp2.137                       1
##  4 10.1007/978-1-4939-3618-2_19           1
##  5 10.1007/978-1-4939-7568-6_14           1
##  6 10.1007/s00394-017-1456-5              1
##  7 10.1007/s10787-017-0401-9              1
##  8 10.1007/s10787-018-0493-x              1
##  9 10.1016/B978-0-12-803239-8.00024-7     1
## 10 10.1016/bs.ampbs.2017.01.002           1
## # … with 242 more rows
```

Yes, which means we need to watch out for errors when finding combinations later on!

### Build relational data

Let's now find combinations (i.e. edges) for each DOI, to construct our graph of relationships.

To do that, we define a function to create all possible combinations for one DOI. Note that we have to cater for:

1. When there is only one institution
2. When there are repeated institutions


```r
get_pairs <- function(df) {
  # store DOI
  DOI <- df$DOI[1]
  # unique institutions in the set
  unique_ins <- unique(df$aff)
  # we only find combinations if there is more than one institution!
  if (length(unique_ins) > 1) {
    pairs <- combn(unique_ins, 2, simplify = FALSE)
    ins1 <- vector(mode = "character", length = length(pairs))
    ins2 <- vector(mode = "character", length = length(pairs))
    for (i in seq_along(pairs)) {
      ins1[i] <- pairs[[i]][1]
      ins2[i] <- pairs[[i]][2]
    }
    # tibble() can repeat values to match length of other columns
    df_out <- tibble(DOI, ins1, ins2)
  } else {
    # if only one affiliation, output one-row dataframe with NAs (of the right type)
    df_out <- tibble(DOI, ins1 = NA_character_, ins2 = NA_character_)
  }
}
```

Let's now use this custom function to get a list of edges:


```r
edges <- wos_clean %>% 
  split(.$DOI) %>% 
  map_dfr(get_pairs)
```

### Export the data

We can save a snapshot of the edges, as well as the list of nodes so we can then populate it with more data outside R.


```r
write_csv(aff_stats, "export/nodes.csv")
write_csv(edges, "export/edges.csv")
```

## Reconciliation with OpenRefine and Wikidata

Here, we need to add location data for institutions, which is probably best done with OpenRefine's reconciliation services with Wikidata.
To keep the visualisation simple, and to save ourselves some work, coordinates added only for institutions that come up more than 3 times.

We can then save our richer list of nodes as "nodes_rec.csv", inside the "export" folder.

## Prepare data for visualisation

First, import our reconciled list of nodes, which now contains spatial data:


```r
nodes <- read_csv("export/nodes_rec.csv") %>% 
  # split coordinates
  separate(`coordinate location`, # the label of the Wikidata property
           into = c("y", "x"),
           sep = ",") %>% 
  # remove potential whitespaces before and after
  mutate_at(vars(x, y), str_trim) %>%
  # convert to numeric (they were imported as chr originally)
  mutate_at(vars(x, y), as.numeric) %>%
  # reorder for matching names to be at the beginning
  select(aff, wd_label, frac_n, n, x, y)
```

We only want to keep some of the most relevant institutions. This is where we decide how busy the graph will look. Let's say they have to have a fractional count of more than 1:


```r
nodes_top <- nodes %>% 
  filter(frac_n > 1)
nrow(nodes_top) # this is the maximum number of nodes there will be on the graph
```

```
## [1] 24
```

Only keep edges that involve these top institutions:


```r
edges_top <- edges %>% 
  filter(ins1 %in% nodes_top$aff, ins2 %in% nodes_top$aff)
```

Let's now move the edges to the beginning, and count how frequent they are:


```r
edges_summ <- edges_top[2:3] %>% # dataframe has to start with the two column defining the edges
  group_by(ins1, ins2) %>% # group the edges and store count as an extra edge attribute
  summarise(n = n()) %>%
  ungroup()
```

## Construct the graph

We use the igraph package to construct an object that contains all the node and edge data:


```r
library(igraph)
graph <- graph_from_data_frame(edges_summ, vertices = nodes_top)
```

This is what the object structure looks like:


```r
graph
```

```
## IGRAPH 8139c75 DN-- 24 110 -- 
## + attr: name (v/c), wd_label (v/c), frac_n (v/n), n (v/n), x (v/n), y
## | (v/n), n (e/n)
## + edges from 8139c75 (vertex names):
## [1] Australian Natl Univ->Griffith Univ                   
## [2] Australian Natl Univ->QIMR Berghofer Med Res Inst     
## [3] Australian Natl Univ->Univ Adelaide                   
## [4] Australian Natl Univ->Univ Calif San Diego            
## [5] Australian Natl Univ->Univ Queensland                 
## [6] Australian Natl Univ->Walter & Eliza Hall Inst Med Res
## [7] Chinese Acad Sci    ->Griffith Univ                   
## + ... omitted several edges
```

It contains both nodes (i.e. "vertices") and edges, with some attributes associated to them.

## Visualise it all

We are using a number of packages for this visualisation:

* ggplot2 as a general visualisation framework
* sf for manipulating spatial data
* rnaturalearth to acquire a basemap from the Natural Earth project
* ggraph for visualising relational data


```r
# load package for network visualisation
library(ggraph)
set_graph_style()
# might need extrafont::font_import() and extrafont::loadfonts(device = "win") to get ggraph's theme Arial Narrow
```

This is the default network graph visualisation in ggraph:


```r
ggraph(graph) +
  geom_edge_link()
```

```
## Using `stress` as default layout
```

![](process_files/figure-html/unnamed-chunk-28-1.png)<!-- -->

The layout defaults to  "stress". We can supply the coordinates for the points, which uses a "manual" layout instead:


```r
ggraph(graph, x = x, y = y) +
  geom_edge_link()
```

![](process_files/figure-html/unnamed-chunk-29-1.png)<!-- -->

This is very hard to read, so using arcs instead of straight lines might help:


```r
ggraph(graph, x = x, y = y) +
  geom_edge_arc()
```

![](process_files/figure-html/unnamed-chunk-30-1.png)<!-- -->

Tweaking this with some extra settings, like transparency and a range of random values for the curvature, can help reading the graph. We also add an extra variable: the number of times the institutions collaborated, visualised as both the width and the colour of the arc.


```r
ggraph(graph, x = x, y = y) +
  geom_edge_arc(aes(width = n, colour = n),
                strength = sample(seq(0.2, 0.5, by = 0.01),
                                  nrow(edges_summ),
                                  replace = TRUE), # variety of values for the bend
                alpha = 0.5, # 50% opacity
                show.legend = FALSE) +
  scale_edge_colour_distiller(palette = "Greens", direction = 1)
```

![](process_files/figure-html/unnamed-chunk-31-1.png)<!-- -->

Let's get a world basemap to use as a background:


```r
# load packages for world basemap
library(rnaturalearth)
library(rgeos)
# get world shapefile for background
world <- ne_countries(scale = "small", returnclass = "sf") %>% 
  filter(admin != "Antarctica") # remove Antarctica from the map
```

An now, we can place the network visualisation on a map:


```r
p <- ggraph(graph, x = x, y = y) +
  geom_sf(data = world, colour = "lightgrey") + # the basemap
  geom_edge_arc(aes(width = n, colour = n),
                strength = sample(seq(0.3, 0.5, by = 0.01),
                                  nrow(edges_summ),
                                  replace = TRUE),
                alpha = 0.5,
                show.legend = FALSE) +
  scale_edge_colour_distiller(palette = "Greens", direction = 1)
p
```

![](process_files/figure-html/unnamed-chunk-33-1.png)<!-- -->

Let's add the nodes and some labels, and focus on the important part of the map:


```r
p  +
  geom_node_point(aes(size = n, col = n), alpha = 0.5, show.legend = FALSE) +
  geom_node_label(aes(label = name, col = n),
                  alpha = 0.8, repel = TRUE,
                  label.padding = 0.2,
                  label.size = 0.1) +
  xlim(min(nodes_top$x), NA) # only show the part of the World that matters
```

![](process_files/figure-html/unnamed-chunk-34-1.png)<!-- -->

We can save the last plot we generated with a command, to use a more suitable size for it:


```r
ggsave(filename = "render/map_1_and_up.png",
       width = 14, height = 7)
```

The larger size will make everything more readable.

And we end up with this:

![](render/map_1_and_up.png)
